# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
[ -r /home/jfinken/.byobu/prompt ] && . /home/jfinken/.byobu/prompt   #byobu-prompt#



###############################################################################
source ~/.bashrc.had
###############################################################################

# added by Miniconda3 4.3.21 installer
# export PATH="/home/jfinken/miniconda3/bin:$PATH"  # commented out by conda initialize
# export PYTHONPATH="/usr/local/lib/python3.6/site-packages"
# export PYTHONPATH="/home/jfinken/miniconda3/envs/py27/lib/python2.7/site-packages"

# Go
export GOROOT=~/go
export GOPATH=~/projects/go
export PATH=$PATH:$GOROOT/bin
export PATH=$PATH:$GOPATH/bin

# Android Studio
export PATH=$PATH:/usr/local/android-studio/bin

# wifi
nmcli_list() {
    nmcli device wifi list
}
nmcli_conn() {
    nmcli device wifi con $1
}

# i3 lock screen
lock() {
    i3lock
}
e() {
    echo $1 |xclip -sel clipboard
}

# What is the nearest commit that resides on a branch other than the current branch, and which branch is that?
ancestor(){

    git show-branch -a \
    | grep '\*' \
    | grep -v `git rev-parse --abbrev-ref HEAD` \
    | head -n1 \
    | sed 's/.*\[\(.*\)\].*/\1/' \
    | sed 's/[\^~].*//'

}
# Dell Precision 7510 Mouse Stick
# xinput --set-prop "AlpsPS/2 ALPS DualPoint Stick" "Device Accel Constant Deceleration" 5.0

# CUDA!
export CUDA_PATH=/usr/local/cuda
export LD_LIBRARY_PATH=/usr/local/lib
export LD_LIBRARY_PATH=$CUDA_PATH/lib64:$LD_LIBRARY_PATH
export PATH=$CUDA_PATH/bin:$PATH

# Numba with CUDA support:
export NUMBAPRO_NVVM=$CUDA_PATH/nvvm/lib64/libnvvm.so 
export NUMBAPRO_LIBDEVICE=$CUDA_PATH/nvvm/libdevice/ 

coinmon() {
    # curl -s "https://api.coinmarketcap.com/v1/ticker/?convert=$1" | jq ".[0] .price_$1"
    denom=usd
    eth=$(curl -s "https://api.coinmarketcap.com/v1/ticker/?convert=$denom" | jq ".[1] .price_$denom")
    echo "1 ETH: $eth $denom"  | tr -d '"'
}
xsetroot -solid "#333333"

# Node Version Manager..node, ug.
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# pyment is a python docstring generator patch-maker:
# https://github.com/dadadel/pyment

# Android Open Source Project:
PATH=~/projects/android/source.android.com/bin:$PATH

# Oracle JDK8:
# export JAVA_HOME=~/projects/jdk1.8.0_162
# export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
# OpenJDK via https://adoptopenjdk.net/
#   what a giant suckhole Oracle is...
export JAVA_HOME=~/projects/jdk8u275-b01
PATH=$JAVA_HOME/bin:$PATH

# moderately handy: git-completion
# source ~/.git-completion.sh

# Protocol buffers:
PATH=/home/jfinken/projects/protoc/bin:$PATH

pwdc(){
    builtin pwd | xclip -sel clipboard
    builtin pwd
}

export VULKAN_SDK=/home/jfinken/projects/dl_on_mobile/vulkan/1.1.106.0/x86_64
# export ANDROID_NDK=/home/jfinken/projects/dl_on_mobile/android-ndk-r14b
export ANDROID_NDK=/home/jfinken/projects/dl_on_mobile/android-ndk-r17c
export ANDROID_NDK_HOME=$ANDROID_NDK
export NDK_ROOT=$ANDROID_NDK
PATH=$VULKAN_SDK:$ANDROID_NDK:$PATH

# HERE-True and Cyclops raw data hash (for S3 prefixing)
hasher() {
    echo -n $1 | md5sum | cut -c1-8
}

# TBB for gtsam
export TBB_INCLUDE_DIRS=/home/jfinken/projects/intel/tbb/include
export TBB_LIBRARIES=/home/jfinken/projects/intel/tbb/build/linux_intel64_gcc_cc5.4.0_libc2.23_kernel4.10.0_release

# Julia!
JULIA_PATH=/home/jfinken/projects/julia-1.2.0/bin
PATH=$JULIA_PATH:$PATH

# IBM Cloud Services
LANGUAGE_TRANSLATOR_APIKEY=z4mO4Ncf9DATb6qJIfWS_k26ZGvHswdXkH0YUVSfitaM
LANGUAGE_TRANSLATOR_URL=https://gateway.watsonplatform.net/language-translator/api
LANGUAGE_TRANSLATOR_AUTH_TYPE=iam

# Intellij
idea(){
    /home/jfinken/projects/intellij/idea-IC-192.6817.14/bin/idea.sh &
}

# CudaSift cython .so finding the libcudasift.so
# I feel like I should have to do this...
export LD_LIBRARY_PATH=/home/jfinken/miniconda3/envs/py37/lib/python3.7/site-packages:$LD_LIBRARY_PATH

# Conda and cudnn for tensorflow-gpu
export LD_LIBRARY_PATH=/home/jfinken/miniconda3/pkgs/cudatoolkit-10.2.89-hfd86e86_0/lib/:$LD_LIBRARY_PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/jfinken/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/jfinken/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/jfinken/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/jfinken/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# To modify gnome-terminal font size
fontsize(){
    # $1
    # To set:
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ font "Monospace ${1}"

    # To get:
    gsettings get org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ font 
}

# MLflow + the HERE python package
export MLFLOW_TRACKING_URI=here-platform://catalog/hrn:here:data::olp-here:mlflow-experiments-jfinken

# dendron journal
save_journal(){
    git commit -m "entries";
    git push origin master
}

# LS Object Tracking work
# export LD_LIBRARY_PATH=/home/jfinken/projects/here/ce/dps/innovation/jetson_tracker_cpp/lk-optical-flow/src/object-tracking/build:$LD_LIBRARY_PATH

# Darknet core
# export DARKNET_HOME=/home/jfinken/projects/darknet
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DARKNET_HOME

# Aggregation:
export LD_LIBRARY_PATH=/opt/protobuf/lib:$LD_LIBRARY_PATH

# ROS 2 eloquent elusor (ubuntu 18.04)
source /opt/ros/eloquent/setup.bash
source /usr/share/colcon_cd/function/colcon_cd.sh
# export LD_LIBRARY_PATH=/opt/ros/eloquent/lib:$LD_LIBRARY_PATH
# Not sure 
# export _colcon_cd_root=/opt/ros/eloquent/share
