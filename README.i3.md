
## README specific to i3 things

#### Configuration

```bash
# i3 config
mkdir ~/.i3
cp i3.config ~/.i3/config
cp exit_menu.sh ~/.i3/
cp i3status.conf ~/.i3status.conf
```

Alternatively, i3 config can live in `~/.config/`
```
~/.config/i3/config

# But set path to exit_menu.sh accordingly
```

The status bar config appears to always live in `~/.`
```
$ ls ~/.i3status.conf
.i3status.conf
```

#### External Monitor

Run `xrandr` to query for what display is what
```bash
$ xrandr -q
...
eDP-1-1 connected 1920x1080+0+0 ...
HDMI-1-2 connected ...
```

Now set these as workspaces in the `config` file, to get deterministic workspaces:
```
workspace 1 output HDMI-1-2
workspace 2 output HDMI-1-2
workspace 3 output HDMI-1-2
workspace 4 output HDMI-1-2
workspace 5 output HDMI-1-2
workspace 6 output HDMI-1-2
workspace 7 output eDP-1-1
```
