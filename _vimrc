"syntax on

set ignorecase "Ignore case when searching
set smartcase

set hlsearch "Highlight search things

set backspace=indent,eol,start
" set file format to windows/dos
set ffs=unix,dos
set nocompatible

" Set font according to win
"set gfn=Bitstream\ Vera\ Sans\ Mono:h10

if has("gui_running")
  syntax enable "Enable syntax hl

  if has("gui_gtk2")
    set guifont=Inconsolata\ 12
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif

  " GUI is running or is about to start.
  " Maximize gvim window.
  set lines=999 columns=99
  set guioptions-=T
  set t_Co=256
  set background=dark
  colorscheme darkblue 
  set nonu
endif

set expandtab
set shiftwidth=4
set tabstop=4
set smarttab

" statusline
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L\ (%P)%)
set statusline+=col:%03c\                            "Colnr
set laststatus=2 " Always show the statusline

