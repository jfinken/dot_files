dot_files
=========

**Dumping and loading gnome-terminal config**

```
# source system:
$ dconf dump /org/gnome/terminal/legacy/profiles:/ > gnome-terminal-profiles.dconf

# estination system (after transferring the gnome-terminal-profiles.dconf file):
$ dconf load /org/gnome/terminal/legacy/profiles:/ < gnome-terminal-profiles.dconf
```

#### i3 config

The `config` file and `exit_menu.sh` is to be located in:
`~/.i3/`

However the `.i3status.conf` is to be located in: `~/`

#### vim

Drop `autoload/`, `bundle/` and `colors/` into `~/.vim`
E.g.
```bash
jfinken@sparkier:~/.vim$ tree
.
├── autoload
│   └── pathogen.vim
├── bundle
│   └── nerdtree
│       ├── autoload
│       │   ├── nerdtree
│       │   │   └── ui_glue.vim
│       │   └── nerdtree.vim
│       ├── CHANGELOG.md
│       ├── _config.yml
│       ├── doc
│       │   └── NERDTree.txt
│       ├── lib
│       │   └── nerdtree
│       │       ├── bookmark.vim
│       │       ├── creator.vim
│       │       ├── event.vim
│       │       ├── flag_set.vim
│       │       ├── key_map.vim
│       │       ├── menu_controller.vim
│       │       ├── menu_item.vim
│       │       ├── nerdtree.vim
│       │       ├── notifier.vim
│       │       ├── opener.vim
│       │       ├── path.vim
│       │       ├── tree_dir_node.vim
│       │       ├── tree_file_node.vim
│       │       └── ui.vim
│       ├── LICENCE
│       ├── nerdtree_plugin
│       │   ├── exec_menuitem.vim
│       │   ├── fs_menu.vim
│       │   └── vcs.vim
│       ├── plugin
│       │   └── NERD_tree.vim
│       ├── README.markdown
│       ├── screenshot.png
│       └── syntax
│           └── nerdtree.vim
└── colors
    └── molokai.vim
```
