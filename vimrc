
" .vimrc
" Josh Finken
" <josh.finken@gmail.com>
"
" For nerdtree and tagbar
call pathogen#infect()

" For vim-go support:
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"   git clone https://github.com/fatih/vim-go.git ~/.vim/plugged/vim-go
" call plug#begin()
" Add plugins to &runtimepath
" Plug 'fatih/vim-go'
" call plug#end()

" No compatible.  You want to use Vim, not vi
set nocp

" We don't wrap lines, they become a LONG horizontal one (useful)  
" set nowrap 

" Set background to dark to have nicer syntax highlighting.
"set background=dark

" We keep 3 lines when scrolling
set scrolloff=3

" We show vertical and horizontal position
set ruler

" Y = y$ not yy.  More intuative
noremap Y y$

" Tilde (~) acts like an operator (à la 'd' or 'c')
"set top

" Incremental search
"set is

" Ignore case when searching
set ic

" Show matching ()'s []'s {}'s
set sm

" Autoindents
set ai

" Autoindents C code
set cin

" Tabs are 4 spaces long
set tabstop=4

" make a tab be spaces instead of a tab
set expandtab

" special case tabs for makefile
" autocmd FileType make 

" When autoindent does a tab, it's 4 spaces long
set shiftwidth=4

" No need to save to make a :next :previous, etc.
set aw

" C-a and C-e go to beginning/end of line in insert mode (I hate Home and End)
inoremap <C-a> <Home>
inoremap <C-e> <End> 

" I like using C-g à la Emacs in the command line.  Don't ask me why.
" cnoremap <C-g> <Esc>

" No idea what it's for...
set backspace=2

" No bell sound
set noerrorbells

" Put title in title bar
set title

" Smoother changes
set ttyfast

" Tabs are converted to space characters
set et

" Remove autocommands just in case
autocmd!

" When using mutt or slrn, text width=72
"autocmd BufRead  mutt*[0-9]                    set tw=72
"autocmd BufRead  .followup,.article,.letter    set tw=72

" Text files have a text width of 72 characters
" autocmd BufNewFile *.txt                       set tw=72
" autocmd BufRead    *.txt                       set tw=72

" LaTeX configuration is in ~/vim/vim.latex
" autocmd BufNewFile *.tex            source ~/vim/latex.vimrc
" autocmd BufRead    *.tex            source ~/vim/latex.vimrc

" Automatically chmod +x Shell and Perl scripts
autocmd BufWritePost   *.sh             !chmod +x %
autocmd BufWritePost   *.pl             !chmod +x %

" for Vundle (plugin management)
" " first clone: http://github.com/gmarik/vundle.git ~/.vim/vundle.git
"filetype off
"set rtp+=~/.vim/vundle.git/ 
"call vundle#rc()
"
" Then add Bundles here:
"
" original repos on github
"Bundle 'tpope/vim-fugitive'
"Bundle 'Lokaltog/vim-powerline'
"
" for powerline
"set laststatus=2 " Always show the statusline

" We put syntax highlighting (COLORS!!)
syntax on

" more clojure
" execute pathogen#infect()

" statusline
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L\ (%P)%)
set statusline+=col:%03c\                            "Colnr
set laststatus=2 " Always show the statusline

"============================
" GOLANG!
"============================

" change the leader to ,
"let mapleader = ","

syntax enable  
filetype plugin on  
" no line numbers
"set number  

let g:go_disable_autoinstall = 0
" writes the content of the file automatically
" if call :make.  vim-go uses this in :GoBuild ...
set autowrite

" make it easier to jump b/t errors in the quickfix list:
"map <C-n> :cn<CR>
"map <C-m> :cp<CR>
"nnoremap <leader>a :cclose<CR>

" shortcuts to build and run a Go program:
"autocmd FileType go nmap <leader>b  <Plug>(go-build)
"autocmd FileType go nmap <leader>r  <Plug>(go-run)

" Highlight
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

" tagbar
let g:tagbar_type_go = {  
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
    \ }
" F8 to toggle tagbar
nmap <F8> :TagbarToggle<CR>

" Ctrl+n to toggle nerd-tree
map <C-n> :NERDTreeToggle<CR>

" molokai color scheme, see ~/.vim/colors
color molokai
" the below is required on my system...
" Make sure you’re using a console terminal capable of 256 colors; not all of
" them do (particularly on mac). You might need to explicitly force Vim to use
" that by doing “set t_Co=256″ on your .vimrc file. - The windows console is
" well… totally unsupported, that only does 16 colors so it’s a mess
set t_Co=256

" better go guru windowing
" set lines=50 columns=100

" navigate between windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

