
#### System Deps 
```bash
sudo apt install aptitude -y
sudo aptitude install vim -y
sudo aptitude install git -y
sudo aptitude install i3 -y
sudo aptitude install byobu -y
sudo aptitude install net-tools -y
sudo aptitude install xclip -y
sudo aptitude install keepassxc -y
sudo aptitude install suckless-tools -y
```


#### Brave 
```bash
# brave
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser
```

#### VSCODE
Fetch and install the vscode deb

#### MINICONDA
- careful w.r.t ROS1 etc.
- miniconda does install to ~/

#### SLACK
Fetch and install the slack deb

#### To set an Ubuntu Desktop background color
To retrieve the currently set wallpaper:
```bash
$ gsettings get org.gnome.desktop.background picture-uri
'file:///usr/share/backgrounds/warty-final-ubuntu.png'

```
To clear the current image setting:
```bash
gsettings set org.gnome.desktop.background picture-uri ""
```
To set the primary color (the first color in a gradient or the solid color):

```bash
# this is a dark gray
gsettings set org.gnome.desktop.background primary-color '#24252b'
```

Or to create a plain color gradient background without messing around making an image:
```bash
gsettings set org.gnome.desktop.background picture-options 'none'
gsettings set org.gnome.desktop.background primary-color '#24252b'
gsettings set org.gnome.desktop.background secondary-color '#33333d'
# or vertical
gsettings set org.gnome.desktop.background color-shading-type 'horizontal'
```

